Role Name
=========

Provisions PostgreSQL Instances on different Operating Systems.

Requirements
------------

Any Ansible capable Host should be all right, the tests are running successfully with:
- Debian:
   `sudo apt install unzip gnupg python3 python3-apt python3-pip python3-setuptools`


Role Variables
--------------

|Variable Name | default | description |
--- | --- | ---
|postgres_major|13|The Major PostgreSQL Version to use|
|postgis_major|3|The Major postgis Version to use|
|postgres_config_dir|OS default|Where the pghba.conf, postgres.conf and other files reside|
|postgres_conf_dir_deb|/etc/postgresql/{{ postgres_major }}/main|Debians default configuration directory|
|postgres_conf_dir_rhel|/var/lib/pgsql/{{ postgres_major }}|RHEL/Fedora default configuration directory|


Dependencies
------------
Ansible community.postgresql collection, either use the install command with `ansible-galaxy collection install community.postgresql`
or add it to the your requirements.yaml
```
---
collections:
  - name: community.postgresql
```


Example Playbook
----------------

Take a look at the test inventory [tests/inventory.yaml](tests/inventory.yaml)

License
-------

Apache License 2.0

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).