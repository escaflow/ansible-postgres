#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
VOLUME_DIR="$(dirname $(pwd)/../)"
PODMAN_NETWORK="devnet" # crete it if not present podman network create devnet
ANSIBLE_CONTAINER="quay.io/ansible/ansible-runner:stable-2.12-latest"
ANSIBLE_LINT_CONTAINER="registry.gitlab.com/pipeline-components/ansible-lint:latest"

# feel free to use another ssh container
CONTAINER_REGISTRY="registry.gitlab.com/escaflow"
CONTAINER_NAME=("container-ssh/debian:latest" "container-ssh/ubuntu:latest" "container-ssh/fedora:latest")

podman run --name ansible-lint --network "${PODMAN_NETWORK}" \
    -v "${VOLUME_DIR}":/code/ansible-postgres:Z \
    -i ${ANSIBLE_LINT_CONTAINER} /bin/sh < "${SCRIPT_DIR}"/lint_script

for CONTAINER in "${CONTAINER_NAME[@]}"
do
    echo "Creating container ${PODMAN_CONTAINER_NAME}"
    PODMAN_CONTAINER_NAME="$(echo "${CONTAINER}"|cut -d "/" -f 2 |cut -d ":" -f 1)"
    podman run --detach --name "ansible-postgres-${PODMAN_CONTAINER_NAME}" --network "${PODMAN_NETWORK}" \
        "${CONTAINER_REGISTRY}/${CONTAINER}"
done

podman run --name build --network "${PODMAN_NETWORK}" \
    -v "${VOLUME_DIR}":/code/ansible-postgres:Z \
    -i "${ANSIBLE_CONTAINER}" /bin/bash < "${SCRIPT_DIR}"/build_script

for CONTAINER in "${CONTAINER_NAME[@]}"
do
    PODMAN_CONTAINER_NAME="$(echo "${CONTAINER}"|cut -d "/" -f 2 |cut -d ":" -f 1)"
    echo "Removing container ${PODMAN_CONTAINER_NAME}"
    podman container rm "ansible-postgres-${PODMAN_CONTAINER_NAME}" -f -v
done

podman container rm build ansible-lint -f -v
