#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
VOLUME_DIR="$(dirname $(pwd)/../)"
PODMAN_NETWORK="devnet" # crete it if not present podman network create devnet
ANSIBLE_CONTAINER="quay.io/ansible/ansible-runner:stable-2.12-latest"
ANSIBLE_LINT_CONTAINER="registry.gitlab.com/pipeline-components/ansible-lint:latest"

# feel free to use another ssh container
CONTAINER_REGISTRY="registry.gitlab.com/escaflow"
CONTAINER_NAME=("container-ssh/debian:latest" "container-ssh/ubuntu:latest" "container-ssh/fedora:latest")

podman run --name ansible-lint --network "${PODMAN_NETWORK}" \
    -v "${VOLUME_DIR}":/code/ansible-postgres:Z \
    -i ${ANSIBLE_LINT_CONTAINER} /bin/sh < "${SCRIPT_DIR}"/lint_script
podman container rm ansible-lint -f -v
for CONTAINER in "${CONTAINER_NAME[@]}"
do
    podman run --detach --name ansible-postgres --network "${PODMAN_NETWORK}" \
        "${CONTAINER_REGISTRY}/${CONTAINER}"
    podman run --name build --network "${PODMAN_NETWORK}" \
        -v "${VOLUME_DIR}":/code/ansible-postgres:Z \
        -i "${ANSIBLE_CONTAINER}" /bin/bash < "${SCRIPT_DIR}"/build_script
    podman container rm ansible-postgres build -f -v
done
